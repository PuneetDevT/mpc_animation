﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

public class BlendAnimation : MonoBehaviour {
    public AnimationClip[] animationClips;
    public Animator animator;
    protected AnimatorOverrideController animatorOverrideController;

    private int animIndex;

    // Use this for initialization
    void Start() {

        animator.GetComponent<Animator>();
        animIndex = -1;
        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = animatorOverrideController;
    }

    // Update is called once per frame
    void Update() {
        float horizontalInput = Input.GetAxis("Horizontal");

        animator.SetFloat("Blend", horizontalInput, 1, Time.deltaTime);

        animIndex = (animIndex + 1) % animationClips.Length;
        
        if (Input.GetKeyDown(KeyCode.S))
        {
          animatorOverrideController["HumanoidWalk"] = animationClips[animIndex];
        }
    }
}
